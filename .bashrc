if [ -d $HOME/.local/bin ]; then
   PATH="$PATH:$HOME/.local/bin"
fi

# install virtualenvwrapper using pip install virtualenvwrapper --user
if [ -f $HOME/.local/bin/virtualenvwrapper.sh ]; then
    # Locate the global Python where virtualenvwrapper is installed.
    if [ "$VIRTUALENVWRAPPER_PYTHON" = "" ]; then
        VIRTUALENVWRAPPER_PYTHON="$(command \which python3)"
    fi
    export WORKON_HOME=$HOME/.virtualenvs
    source $HOME/.local/bin/virtualenvwrapper.sh
fi

if [ -d $HOME/.pyenv ]; then
    export PYENV_ROOT="$HOME/.pyenv"
    export PATH="$PATH:$PYENV_ROOT/bin"
fi


if [ -d $HOME/.nvm ]; then
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
if [ -d $HOME/.rvm ]; then
    export PATH="$PATH:$HOME/.rvm/bin"
    # Load RVM into a shell session *as a function*
    [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
fi

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/bin"
[ -d $HOME/.rvm ] && export PATH="$PATH:$HOME/go/bin"

# K8 plugins
# curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
# source <(kubectl completion bash)

# K8 plugins
# [ -d $HOME/.krew ] && export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"
