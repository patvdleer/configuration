# Configuration
This repo is to keep some of my configuration at a place where it's easily reachable (I can fetch it using curl/wget).
## Appending to files
Append configs to your files
```bash
curl https://gitlab.com/patvdleer/configuration/-/raw/master/.bashrc >> ~/.bashrc
curl https://gitlab.com/patvdleer/configuration/-/raw/master/.screenrc >> ~/.screenrc
curl https://gitlab.com/patvdleer/configuration/-/raw/master/.gitconfig >> ~/.gitconfig
```


## Autocomplete

```bash
sudo curl \
    -L https://raw.githubusercontent.com/docker/compose/1.29.2/contrib/completion/bash/docker-compose \
    -o /etc/bash_completion.d/docker-compose
    
sudo kubectl completion bash > /etc/bash_completion.d/kubectl
```
